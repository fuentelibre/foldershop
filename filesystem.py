import os
import sys
import shutil
import pyinotify
import asyncio
from pathlib import Path


class WuySocketsFS:

    def __init__(self):
        self.root = os.path.realpath(sys.path[0])
        self.wm = pyinotify.WatchManager()
        self.mask = pyinotify.IN_DELETE | pyinotify.IN_CREATE | pyinotify.IN_MODIFY
        target = os.path.join(sys.path[0], "web/")
        self.wm.add_watch(target, self.mask, rec=True)
        loop = asyncio.get_event_loop()
        self.event_notifier = pyinotify.AsyncioNotifier(self.wm, loop,
                                                        default_proc_fun=live_handler(self))

    def listDir(self):
        files = [{'title': f.name,
                  'data': {'path': f.path},
                  'isDraggable': False,
                  'isLeaf': False}
                 for f in os.scandir(self.root)
                 if f.is_dir() and not f.name.startswith('.')]
        return [{'title': os.path.basename(self.root),
                 'data': {'path': self.root},
                 'isDraggable': False,
                 'free': shutil.disk_usage(self.root)[2],
                 'children': files}]

    def listFiles(self, path):
        try:
            if Path(path).relative_to(self.root):
                files = [{'name': f.name,
                          'size': f.stat()[6],
                          'type': 'file',
                          'ext':os.path.splitext(f.name)[-1][1:]}
                         for f in os.scandir(path)
                         if not f.is_dir() and not f.name.startswith('.')]
                return files
        except ValueError:
            return(self.listFiles(self.root))


class live_handler(pyinotify.ProcessEvent):

    def __init__(self, index):
        self.index = index
        pyinotify.ProcessEvent.__init__(self)

    def process_IN_CREATE(self, event):
        if not event.dir:
            print("Got new file: ", event.pathname)

    def process_IN_MODIFY(self, event):
        if not event.dir:
            print("Modify file: ", event.pathname)

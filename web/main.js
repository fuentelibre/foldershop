import Vue from 'vue/dist/vue.common'
import VueFileAgent from 'vue-file-agent'
import VueCodemirror from 'vue-codemirror'
import * as cm_active_line from 'codemirror/addon/selection/active-line.js'
import SlVueTree from 'sl-vue-tree'
import utils from 'vue-file-agent/src/lib/utils.js'

Vue.use(VueCodemirror)
Vue.use(VueFileAgent)
Vue.component('sl-vue-tree', SlVueTree);
Vue.component('VueFilePreview', VueFileAgent.VueFilePreview);
window.getSizeFormatted = utils.getSizeFormatted

startUp(Vue)
new Vue({el:"#app"})

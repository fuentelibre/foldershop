import os
import sys
import wuy
import vbuild

import filesystem


class index(wuy.Server, filesystem.WuySocketsFS):

    def __init__(self):
        filesystem.WuySocketsFS.__init__(self)
        wuy.Server.__init__(self)

    def beep(self):
        print("\a BEEP !!!")

    def _render(self, path):
        template = open("web/template.html").read()
        self.v = vbuild.render(os.path.join(sys.path[0], "web/*.vue"))
        template = template.replace("<!-- SCRIPT -->", str(self.v.script))
        template = template.replace("<!-- STYLE -->", str(self.v.style))
        return template.replace("<!-- HTML -->", str(self.v.html))


index()
